#[macro_use]
extern crate bencher;

use bencher::Bencher;

const CHAR: char = '>';
const CHARSET: &str = "abcdefghijklmnopqrstuvwxyz1234567890_!?=<>";

fn hashset(bench: &mut Bencher) {
    use std::collections::HashSet;
    let hash = CHARSET.chars().collect::<HashSet<_>>();
    bench.iter(|| hash.contains(&CHAR))
}

fn contains(bench: &mut Bencher) {
    // contains は str には使えないので Vec<char> を作る
    let vec = CHARSET.chars().collect::<Vec<_>>();
    bench.iter(|| vec.contains(&CHAR));
}

fn contains_binary_search(bench: &mut Bencher) {
    let mut vec = CHARSET.chars().collect::<Vec<_>>();
    vec.sort();
    bench.iter(|| vec.binary_search(&CHAR).is_ok());
}

fn is_alphanumeric(bench: &mut Bencher) {
    // 今回は小文字しか扱わないので `is_ascii_alphanumeric` は直接には使わない
    bench.iter(|| {
        (CHAR.is_ascii_alphabetic() && CHAR.is_ascii_lowercase())
            || CHAR.is_digit(10)
            || CHAR == '_'
            || CHAR == '!'
            || CHAR == '?'
            || CHAR == '='
            || CHAR == '<'
            || CHAR == '>'
    });
}

fn regex(bench: &mut Bencher) {
    use regex::Regex;
    let regex = Regex::new("[a-zA-Z0-9_!?=<>]").unwrap();
    let c = CHAR.to_string();

    bench.iter(|| regex.is_match(&c))
}

benchmark_group!(
    benches,
    hashset,
    contains,
    contains_binary_search,
    is_alphanumeric,
    regex
);
benchmark_main!(benches);
