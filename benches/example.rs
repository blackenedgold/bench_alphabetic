#[macro_use]
extern crate bencher;

use bencher::Bencher;

fn hashset(bench: &mut Bencher) {
    use std::collections::HashSet;
    let hash = "abcdefghijklmnopqrstuvwxyz".chars().collect::<HashSet<_>>();
    bench.iter(|| hash.contains(&'m'))
}

fn contains(bench: &mut Bencher) {
    const N: usize = 1024;
    let vec = "abcdefghijklmnopqrstuvwxyz".chars().collect::<Vec<_>>();
    bench.iter(|| vec.contains(&'m'));

    bench.bytes = N as u64;
}

fn is_alphabetic(bench: &mut Bencher) {
    bench.iter(|| 'm'.is_alphabetic());
}

benchmark_group!(benches, hashset, contains, is_alphabetic);
benchmark_main!(benches);
