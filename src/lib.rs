const CHARSET: &str = "abcdefghijklmnopqrstuvwxyz1234567890_!?=<>";

pub fn hashset(c: &char) -> bool {
    use std::collections::HashSet;
    let hash = CHARSET.chars().collect::<HashSet<_>>();
    hash.contains(&c)
}

pub fn contains(c: &char) -> bool {
    let vec = CHARSET.chars().collect::<Vec<_>>();
    vec.contains(&c)
}

pub fn is_alphanumeric(c: &char) -> bool {
    c.is_alphanumeric()
        || c == &'_'
        || c == &'!'
        || c == &'?'
        || c == &'='
        || c == &'<'
        || c == &'>'
}
